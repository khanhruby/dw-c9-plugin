define(function(require, exports, module) {
    main.consumes = [
        "Plugin", "ui", "menus", "preferences", "settings", "proc",
        "save", "dialog.error", "console", "tree",
        "fs", "language"
    ];
    main.provides = ["demandware"];
    return main;

    function main(options, imports, register) {
        var Plugin      = imports.Plugin;
        var ui          = imports.ui;
        var menus       = imports.menus;
        var settings    = imports.settings;
        var prefs       = imports.preferences;
        var proc        = imports.proc;
        var dwConsole   = imports.console;
        var save        = imports.save;
        var showError   = imports['dialog.error'].show;
        var workingTree = imports.tree;
        var fs          = imports.fs;
        var language    = imports.language;

        /***** Initialization *****/
        language.registerLanguageHandler('plugins/demandware/dw-infer/infer_completer');

        var plugin = new Plugin("Demandware.com", main.consumes);
        var emit = plugin.getEmitter();
        emit.setMaxListeners(1000);
        var TYPE = "Javascript";
        
        function load() {
            // add 'Demandware' menu item to 'Tools' top level menu
            menus.addItemByPath("Tools/~", new ui.divider(), 999998, plugin);
            menus.addItemByPath("Tools/Demandware", null, 999999, plugin);
            
            var toolsUploadCartridgeMenuItem = new ui.item({
                onclick: function(){ workingTree.selection.forEach(function(node){
                        upload(node);
            })}});
            toolsUploadCartridgeMenuItem.disabled = true;
            
            menus.get("Tools").menu.addEventListener('display',function(m){
                workingTree.selection.forEach(function(node){
                    fs.exists(node+"/cartridge", function(exists){
                        toolsUploadCartridgeMenuItem.setAttribute("disabled", !exists);
                    });
                });
            });
            
            var toolsUploadAllCartridgesMenuItem = new ui.item({
                onclick: function(){ 
                        fs.readdir("/", function (err, files) {
                            files.forEach(function(file) {
                                fs.exists("/"+file.name+"/cartridge", function(exists){
                                    if (exists) {
                                        upload("/"+file.name);
                                    }
                                });
                            })
                        })
                }});

            menus.addItemByPath("Tools/Demandware/Upload Cartridge", toolsUploadCartridgeMenuItem, 100, plugin);
            menus.addItemByPath("Tools/Demandware/Upload All Cartridges", toolsUploadAllCartridgesMenuItem, 200, plugin);

            // Define settings
            settings.on("read", function(e){
                settings.setDefaults("project/my-demandware", [
                    ["version", "version1"],
                    ["enabled", true]
                ]);
            });
            
            prefs.add({
                "Project" : {
                    "Demandware Server Upload" : {
                        position: 500,
                        "Hostname": {
                            type: "textbox",
                            setting: "project/my-demandware/@hostname",
                            position: 100
                        },
                        "Username": {
                            type: "textbox",
                            setting: "project/my-demandware/@username",
                            position: 200
                        },
                        "Password": {
                            type: "password",
                            setting: "project/my-demandware/@password",
                            position: 300
                        },
                        "Version": {
                            type: "textbox",
                            setting: "project/my-demandware/@version",
                            position: 400
                        },
                        "Enabled": {
                            type: "checkbox",
                            setting: "project/my-demandware/@enabled",
                            position: 500
                        }
                    }
                }
            }, plugin);
            
            // create and open the Demandware console
            var demandwareTab;                    
            dwConsole.open({path:'demandware.console',title:'Demandware', 
                active:true, 
                document: {
                    meta: {
                        newfile: true
                    },
                    title:'Demandware'
                }}, function(error, tab) { 
                    tab.document.value = 'Welcome to Demandware Cloud 9 Plugin\n';
                    demandwareTab = tab;
                    demandwareTab.editor.setOption('syntax', 'ace/mode/properties');
        
                });
            
            // Create a logger which uses the console above
            var logger = {
                log : function(message){
                    demandwareTab.document.value += (new Date()).toLocaleString().replace(new RegExp(':', 'g'),'.') + ": "+  message.replace(/^\s+|\s+$/g, "") +"\n";
                    var scrollLine = demandwareTab.document.value.split('\n').length
                    if (scrollLine > 5) {
                        scrollLine -= 2;
                    }
                    demandwareTab.editor.scrollTo(scrollLine,0);
                },
                error : function(message){
                    showError(message);
                    this.log("ERROR " + message);
                },
                info : function(message){
                    this.log(message);
                },
                debug : function(message){
                    this.log(message);
                },
                fatal : function(message){
                    this.log(message);
                },
                warn  : function(message){
                    this.log(message);
                }
            }
            
            /**
             * Uploads a given local path to the remote server
             * 
             * Uses the settings of the plugin and spawns a node process
             */
            var upload = function(path){
                proc.spawn("node", {
                    args: [".c9/plugins/demandware/node_modules/dw-node-code-uploader/lib/main",
                        '/home/ubuntu/workspace/'+path,
                        settings.get("project/my-demandware/@hostname"),
                        settings.get("project/my-demandware/@version"),
                        settings.get("project/my-demandware/@username"),
                        settings.get("project/my-demandware/@password")
                    ]
                }, function(err, process) {
                    if (err) return console.error(err);
                    
                    process.stdout.on("data", function(chunk) {
                        console.log(chunk); 
                        logger.info(chunk); 
                    });
                    
                    process.stderr.on("data", function(chunk) {
                        console.error(chunk);    
                        logger.error(chunk);
                    });
                    
                    process.on("exit", function(code) {
                        if(code == 0){
                            //logger.info("Save successfully finished");
                        }else{
                            logger.error("Server upload failed!");
                        }
                        console.log("Process Stopped ("+code+")"); 
                    });
                });
            }

            // event handler to upload saved files
            save.on("afterSave",function(e){
                if (e.path.indexOf('/cartridge/') > -1) {
                    upload(e.path);
                }
            });

            var uploadCartridgeMenuItem;

            workingTree.getElement('container',function(elem){
                var mnuCtxTree = elem.getAttribute('contextmenu');
                
                menus.addItemToMenu(mnuCtxTree, new ui.divider({}), 2000, plugin);

                uploadCartridgeMenuItem = new ui.item({
                    match: "folder",
                    write: true,
                    disabled: true,
                    caption: "Upload Cartridge",
                    onclick: function(){ workingTree.selection.forEach(function(node){
                        upload(node);
                    }); }
                });
                menus.addItemToMenu(mnuCtxTree, uploadCartridgeMenuItem, 2100, plugin);
                // update visibility of "Upload cartridge" when menu becomes visible
                mnuCtxTree.addEventListener('prop.visible',function(m){
                    workingTree.selection.forEach(function(node){
                        fs.exists(node+"/cartridge", function(exists){
                            uploadCartridgeMenuItem.setAttribute("disabled", !exists);
                            emit("menuUpdate", { node: workingTree.selectedNode, menu: mnuCtxTree });
                        });
                    });
                    
                });
            });
            
        }

        /***** Lifecycle *****/
        plugin.on("load", function() {
            load();
        });
        
        plugin.on("unload", function() {
        });
        
        register(null, {
            "demandware": plugin
        });
    }
});