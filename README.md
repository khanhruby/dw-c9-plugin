# Demandware Server Upload Plugin
This plugin will upload Demandware cartridges to a configured server. 

Install via `c9 install demandware`.

N.B. : Quick workaround when you're faced to that issue :

```
#!bash
Command failed: /home/ubuntu/.c9/node/bin/npm
```

If you check on the folder `/home/ubuntu/.c9` you’ll see that there is no "node" folder.

In order to resolve that issue, you've to follow that process below :

```
#!bash
$ cd /tmp
$ git clone https://github.com/c9/core/ cloud9
$ cd cloud9
$ cd scripts/
$ ./install-sdk.sh
$ cd /home/ubuntu/.c9/node
```

Now, you can try the following command :

```
#!bash
$ c9 install demandware
```

*Hint:* Right-click a cartridge in the navigation should now show _Upload cartridge_ as a new option.

# Configuration

Navigate to *Cloud 9* -> *Preferences* -> *Demandware* section and enter sandbox hostname and credentials.

# License
This plugin is provided under MIT license.

Enjoy!